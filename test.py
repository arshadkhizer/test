#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import json

tmpl_json = {
"MoreAssestsType":0,
"FillterRoomNum":0,
"GridDisplayType":0,
"ResultLable":"גבעתיים",
"ResultType":1,
"ObjectID":"6300",
"ObjectIDType":"number",
"ObjectKey":"SETL_CODE",
"DescLayerID":"SETL_MID_POINT",
"Alert":"",
"X":182247.64,
"Y":664183.71,
"Gush":"",
"Parcel":"",
"showLotParcel":False,
"showLotAddress":False,
"OriginalSearchString":"גבעתיים ",
"MutipuleResults":False,
"ResultsOptions":"",
"CurrentLavel":2,
"Navs":[{"text":"מחוז תל אביב - יפו", "url":"", "order":1}],
"QueryMapParams":{
	"QueryToRun":"",
	"QueryObjectID":"6300",
	"QueryObjectType":"number",
	"QueryObjectKey":"SETL_CODE",
	"QueryDescLayerID":"KSHTANN_SETL_AREA",
	"SpacialWhereClause":""
	},
"isHistorical":False,
"PageNo":1,
"OrderByFilled":"",
"OrderByDescending":True,
"Distance":0
}

def do_req(page_no):
	json_req = tmpl_json.copy()
	json_req['PageNo'] = page_no
	#print json_req
	req = requests.post('https://www.nadlan.gov.il/Nadlan.REST/Main/GetAssestAndDeals', json=json_req)
	return req

def main():
	i = 1
	while True:
		req = do_req(i)
		if req.status_code != 200:
			break
		print req.json()
		data = req.json()
		#file = 'data'+i+
		with open('%i.txt' % i, 'w') as outfile:
			json.dump(data, outfile)
		i += 1

if __name__ == '__main__':
	main()
